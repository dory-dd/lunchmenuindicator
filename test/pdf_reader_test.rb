require 'minitest/autorun'
require "minitest/reporters"
Minitest::Reporters.use! [Minitest::Reporters::SpecReporter.new]

require "../lib/pdf_reader"

class TestTodayLunchIndicator < Minitest::Test
  def setup
    @c = TodayLunchIndicator.new
    @pdfpath = "/tmp/Menu_Cafe\ Lounge\ _Mar\,2019.pdf"
  end
  
  def test_notificationTodayMenu_readJsonMenu
    result = @c.notificationTodayMenu(@c.readJsonMenu)
    assert_match /^(月|火|水|木|金) .+ .+ .+/,result,result
  end
  
  def test_firstdayWarkDay?
    beginYoubi = 6
    result = @c.firstdayWarkDay?
    assert_same 0, result
  end
  
  def test_getDayMenu_pdfRenderer
    result = @c.getDayMenu(@c.pdfRenderer(@pdfpath))
    assert result.size < 22 && result.size > 18,result
  end
  
  def test_getDayMenu_pdfRenderer_youbi
    result = @c.getDayMenu(@c.pdfRenderer(@pdfpath))
    assert_match /^ (月|火|水|木|金) .+ .+ .+/,result[0]
  end
  
  def test_writeMenu_doc
    result = @c.writeMenu(@c.getDayMenu(@c.pdfRenderer(@pdfpath)))
    assert_includes result,"3/1"
  end
  
  
end