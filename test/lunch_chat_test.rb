require 'minitest/autorun'
require "minitest/reporters"
Minitest::Reporters.use! [Minitest::Reporters::SpecReporter.new]

require "../lib/lunch_chat"


class TestConnect < Minitest::Test
  def setup
    @c = ConnectChatWork.new
  end
  
  def test_generateMenuPdfUrl
    result = @c.generateMenuPdfUrl
    url = URI.split(result)
    url = url[2].to_s
    assert url =~ /tky-chat-work-appdata/,url
  end
  
  def test_opens3uri
    result = @c.opens3uri.meta["content-length"].to_i
    assert result <= 1000000,result 
  end
  
end