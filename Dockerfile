FROM ruby

LABEL MAINTAINER = "hkobayashi"
ADD . /lunch-menu-chaser
WORKDIR /lunch-menu-chaser
COPY Gemfile Gemfile.lock ./
RUN apt-get update -qq && apt-get install -y
RUN bundle update --bundler
RUN bundle install


CMD ["bundle", "exec", "rackup", "config.ru","-o", "0.0.0.0"]
