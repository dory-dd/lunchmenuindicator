require "pdf-reader"
require "business_time"
require "json"

class FromPdfGenarateJson
  ThisYear = Time.now.year
  ThisMonth = Time.now.month
  BeginYoubi = Time.current.beginning_of_month.wday
  LunchMenuJsonFile = "./json/#{ThisYear}_#{'%02d' %ThisMonth}_Lunch_Menu.json"
  # Pdfpath = "/tmp/Menu_Cafe Lounge _Apr,2019.pdf"
  
  def initialize(pdfpath)
    @pdfpath = pdfpath

    puts writeJson
  end

  def pdfRenderer
    PDF::Reader.new(@pdfpath)
  end
  
  def firstdayWarkDay?
    if BeginYoubi == 6
      firstday = 2
    elsif BeginYoubi == 0
      firstday = 1
    else
      firstday = 0
    end
    firstday
  end
  
  def getDayMenu(pdfRenderer)
    slicedDocument = []
    pdfRenderer.pages.each do |page|
      slicedDocument = page.text.gsub(/([!-~]|材料.*。)/,"").squeeze.to_s.split("\n").compact.uniq.slice(3..-1)
    end
    slicedDocument
  end
  
  def writeMenu(doc)
    day = firstdayWarkDay?
    dayMenuHash = {}
    doc.each_with_index do |menu,i|
      if menu =~ /^\s月/ && i == 0
        day += 1
      elsif menu =~ /^\s月/ && i >= 1
        day += 3
      else
        day += 1
      end
      menu.gsub!(/(月|火|水|木|金)/,"")
      dayMenuHash.store("#{ThisMonth}/#{day}","#{menu.lstrip}")
    end
    dayMenuHash
  end
  
  def writeJson
    if File.exist?(LunchMenuJsonFile)
      "false"
    else
      File.open(LunchMenuJsonFile, 'w') do |file|
        file.puts(JSON.pretty_generate(writeMenu(getDayMenu(pdfRenderer))))
      end
      "true"
    end
  end
end