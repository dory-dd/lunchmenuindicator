# coding: utf-8

require 'sinatra'
require 'sinatra/reloader'
require 'json'
#require_relative '../lib/lunch_chat.rb '
require_relative './lib/pdf_reader.rb'

#set bind
# set :bind '0.0.0.0'

###Set Json Path ###
set :Public_folder, File.dirname(__FILE__) + '/json'

###define Hepler###
helpers do
  @succsess =<<-EOS
  -if @mes == "true"
     .alert.alert-dismissible.alert-success
       %button.close{"data-dismiss" => "alert", :type => "button"}
       %strong アップロードに成功しました。
  EOS

  @failed =<<-EOS
     -elsif @mes1 == "false"
      .alert.alert-dismissible.alert-danger
        %button.close{"data-dismiss" => "alert", :type => "button"} ×
        %strong ファイルは既に存在します
   EOS

end


### difine method ###
def jsonRead
  files_name = Dir.glob("json/*")
  @files_path = []
  files_name.each do |file|
    @files_path << file
  end
  filepath=@files_path.sort[-1]
  #なんかわからんが動いてるからヨシ！
  File.open(filepath) do |files|
    @menuhash = JSON.load(files)
  end
end

#最新のメニューの一覧表示
get '/' do
  @title = "TYO2 Lunch Menu Chaser"
  #jsonを読み込み
  jsonRead

  @today = Time.new.strftime("%m/%d").delete("0")

  haml :index
end

# アップロード処理
post '/upload' do
  if params[:file]

    save_path = "./pdf/#{params[:file][:filename]}"

    File.open(save_path, 'wb') do |f|
      p params[:file][:tempfile]
      f.write params[:file][:tempfile].read
      @mes = "true"
      @mes1 = FromPdfGenarateJson.new(params[:file][:tempfile])
    end

  else
    @mes = "false"
  end
  redirect back
end